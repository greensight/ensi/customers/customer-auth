<?php

namespace App\Domain\Users\Models\Tests\Factories;

use App\Domain\Users\Models\ConfirmationCode;
use App\Domain\Users\Models\User;
use Ensi\LaravelTestFactories\BaseModelFactory;

class ConfirmationCodeFactory extends BaseModelFactory
{
    protected $model = ConfirmationCode::class;

    public function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'user_id' => User::factory(),
            'phone' => $this->faker->numerify('+7##########'),
            'value' => $this->faker->numerify('####'),
            'attempt' => $this->faker->numberBetween(1, 3),
            'failed_checks_count' => $this->faker->numberBetween(1, 3),
            'end_at' => $this->faker->dateTime(),
            'verified_at' => $this->faker->nullable()->dateTime(),
            'expired_at' => $this->faker->dateTime(),
        ];
    }

    public function forRegistration(): self
    {
        return $this->active()->state([
            'user_id' => null,
        ]);
    }

    public function active(): self
    {
        return $this->state([
            'attempt' => 1,
            'failed_checks_count' => 0,
            'end_at' => $this->faker->dateTimeBetween(now(), '+1 minute'),
            'expired_at' => $this->faker->dateTimeBetween(now(), '+2 minute'),
        ]);
    }

    public function expired(): self
    {
        return $this->state([
            'end_at' => $this->faker->dateTimeBetween('+1 minute', '+2 minute'),
            'expired_at' => $this->faker->dateTime('-1 minute'),
        ]);
    }

    public function blocked(int $attempt = 1): self
    {
        return $this->state([
            'attempt' => $attempt,
            'end_at' => $this->faker->dateTime('-1 minute'),
            'expired_at' => $this->faker->dateTime('-1 minute'),
        ]);
    }
}
