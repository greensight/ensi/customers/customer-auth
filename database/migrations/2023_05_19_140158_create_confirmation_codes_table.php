<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('confirmation_codes', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->string('phone')->unique();
            $table->string('value', 4);
            $table->smallInteger('attempt')->default(0);
            $table->smallInteger('failed_checks_count')->default(0);
            $table->timestamp('end_at', 6)->nullable();
            $table->timestamp('verified_at', 6)->nullable();
            $table->timestamp('expired_at', 6);
            $table->timestamps(6);

            $table->index(['user_id']);

            $table->foreign('user_id')
                ->references('id')
                ->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('confirmation_codes');
    }
};
