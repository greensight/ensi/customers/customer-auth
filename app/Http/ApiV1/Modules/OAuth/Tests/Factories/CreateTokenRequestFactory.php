<?php

namespace App\Http\ApiV1\Modules\OAuth\Tests\Factories;

use App\Http\ApiV1\OpenApiGenerated\Enums\GrantTypeEnum;
use Ensi\LaravelTestFactories\BaseApiFactory;
use Laravel\Passport\Client;

class CreateTokenRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        $client = Client::factory()->asPasswordClient()->create();

        $grandType = $this->faker->randomEnum(GrantTypeEnum::cases());

        return [
            'grant_type' => $grandType,
            'client_id' => $client['id'],
            'client_secret' => $client['secret'],
            'scope' => [],
            ...($grandType == GrantTypeEnum::PASSWORD ? $this->generateAsPassword() : []),
            ...($grandType == GrantTypeEnum::CONFIRMATION_CODE_BY_PHONE ? $this->generateAsConfirmationCodeByPhone() : []),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }

    public function asPassword(?string $username = null, ?string $password = null): self
    {
        return $this->state([
            'grant_type' => GrantTypeEnum::PASSWORD->value,
            ...$this->generateAsPassword($username, $password),
        ]);
    }

    protected function generateAsPassword(?string $username = null, ?string $password = null): array
    {
        return array_merge($this->defaultValues(), [
            'username' => $username ?: $this->faker->email(),
            'password' => $password ?: $this->faker->word(),
        ]);
    }

    public function asConfirmationCodeByPhone(?string $phone = null, ?string $code = null): self
    {
        return $this->state([
            'grant_type' => GrantTypeEnum::CONFIRMATION_CODE_BY_PHONE->value,
            ...$this->generateAsConfirmationCodeByPhone($phone, $code),
        ]);
    }

    protected function generateAsConfirmationCodeByPhone(?string $phone = null, ?string $code = null): array
    {
        return array_merge($this->defaultValues(), [
            'phone' => $phone ?: $this->faker->numerify('+7##########'),
            'code' => $code ?: $this->faker->numerify('####'),
        ]);
    }

    protected function defaultValues(): array
    {
        return [
            'username' => null,
            'password' => null,
            'phone' => null,
            'code' => null,
        ];
    }
}
