<?php

namespace App\Domain\Users\Models;

use App\Domain\Users\Models\Tests\Factories\UserFactory;
use Carbon\CarbonInterface;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Laravel\Passport\HasApiTokens;

/**
 * @property array $attributes
 *
 * @property int $id
 * @property string|null $login
 * @property string|null $password
 * @property bool $active
 * @property string|null $password_token
 * @property CarbonInterface|null $password_token_created_at
 * @property string|null $confirmation_code
 * @property CarbonInterface|null $confirmation_code_created_at
 *
 * @property CarbonInterface $created_at
 * @property CarbonInterface $updated_at
 */
class User extends Authenticatable
{
    use HasApiTokens;
    use Notifiable;

    /**
     * Lifetime for confirmation code (in days)
     */
    public const LIFE_TIME_PASSWORD_TOKEN = 3;

    /**
     * Lifetime for confirmation code (in days)
     */
    public const LIFE_TIME_CONFIRMATION_CODE = 3;

    protected $fillable = ['login', 'password', 'active'];

    protected $hidden = ['password'];

    protected $attributes = [
        'active' => true,
    ];

    public function setPasswordAttribute(?string $value): void
    {
        $this->attributes['password'] = $value ? self::encryptPassword($value) : null;
    }

    public static function encryptPassword(string $password): string
    {
        return Hash::make($password);
    }

    /**
     * @param $password
     * @return bool
     */
    public function checkPassword($password)
    {
        return Hash::check($password, $this->password);
    }

    public function generatePasswordToken(): void
    {
        $this->password_token = md5($this->login . time());
        $this->password_token_created_at = now();
    }

    public function destroyPasswordToken(): void
    {
        $this->password_token = null;
        $this->password_token_created_at = null;
    }

    /**
     * Generate new confirmation code for sms
     */
    public function generateConfirmCode(): void
    {
        $this->confirmation_code = substr(str_shuffle('0123456789'), 0, 4);
        $this->confirmation_code_created_at = now();
    }

    public function checkConfirmCode(string $code): bool
    {
        return $this->confirmation_code == $code;
    }

    public function destroyConfirmCode(): void
    {
        $this->confirmation_code = null;
        $this->confirmation_code_created_at = null;
    }

    /**
     * Override the field which is used for username in the Laravel Passport authentication
     *
     * @param string $login
     */
    public function findForPassport(string $login)
    {
        return $this->where('login', $login)->first();
    }

    /**
     * Add a password validation callback for Laravel Passport
     *
     * @param $password
     * @return bool Whether the password is valid
     */
    public function validateForPassportPasswordGrant($password)
    {
        return $this->checkPassword($password);
    }

    public static function findByPasswordToken(string $passwordToken): ?User
    {
        return User::where('password_token', $passwordToken)->first();
    }

    public static function factory(): UserFactory
    {
        return UserFactory::new();
    }
}
