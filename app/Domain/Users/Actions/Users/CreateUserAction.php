<?php

namespace App\Domain\Users\Actions\Users;

use App\Domain\Users\Actions\Users\Data\CreateUserData;
use App\Domain\Users\Actions\Users\Data\RegisterRequestData;
use App\Domain\Users\Models\User;

class CreateUserAction
{
    public function execute(RegisterRequestData|CreateUserData $data): User
    {
        $user = new User();
        $user->fill($data->toArray());
        $user->save();

        return $user;
    }
}
