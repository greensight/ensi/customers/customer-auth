<?php

namespace App\Domain\Users\Actions\Users\Data;

use App\Http\ApiV1\OpenApiGenerated\Enums\RegisterValidationTypeEnum;

class RegisterRequestData
{
    public function __construct(
        public RegisterValidationTypeEnum $validationType,
        public string $login,
        public string $firstName,
        public string $phone,
        public ?string $password = null,
        public ?string $code = null,
        public ?string $lastName = null,
        public ?string $birthday = null,
        public ?bool $active = null
    ) {
    }

    public function toArray(): array
    {
        return [
            'validation_type' => $this->validationType,
            'login' => $this->login,
            'password' => $this->password,
            'code' => $this->code,
            'first_name' => $this->firstName,
            'last_name' => $this->lastName,
            'phone' => $this->phone,
            'birthday' => $this->birthday,
        ];
    }
}
