<?php

namespace App\Http\ApiV1\Modules\Common\Controllers;

use App\Domain\Common\Actions\DeleteCustomerPersonalDataAction;
use App\Http\ApiV1\Modules\Common\Requests\DeleteCustomerPersonalDataRequest;
use App\Http\ApiV1\Support\Resources\EmptyResource;

class CustomerPersonalDataController
{
    public function delete(DeleteCustomerPersonalDataRequest $request, DeleteCustomerPersonalDataAction $action): EmptyResource
    {
        $action->execute($request->getUserId());

        return new EmptyResource();
    }
}
