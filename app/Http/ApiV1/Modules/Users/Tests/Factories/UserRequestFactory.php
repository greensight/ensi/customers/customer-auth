<?php

namespace App\Http\ApiV1\Modules\Users\Tests\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;

class UserRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'login' => $this->faker->unique()->userName,
            'password' => $this->faker->password(),
            'active' => $this->faker->boolean(),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
