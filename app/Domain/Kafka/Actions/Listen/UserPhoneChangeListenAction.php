<?php

namespace App\Domain\Kafka\Actions\Listen;

use App\Domain\Kafka\Messages\Listen\Dtos\UserDto;
use App\Domain\Kafka\Messages\Listen\UserPhoneChangeEventMessage;
use App\Domain\Users\Models\ConfirmationCode;
use RdKafka\Message;

class UserPhoneChangeListenAction
{
    public function execute(Message $message): void
    {
        $eventMessage = UserPhoneChangeEventMessage::makeFromRdKafka($message);
        /** @var UserDto $userDto */
        $userDto = $eventMessage->attributes;

        /** @var ConfirmationCode|null $confirmationCode */
        $confirmationCode = ConfirmationCode::query()->where('user_id', $userDto->user_id)->first();

        if (empty($confirmationCode)) {
            return;
        }

        $confirmationCode->phone = $userDto->phone;
        $confirmationCode->save();
    }
}
