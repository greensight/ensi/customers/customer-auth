<?php

namespace App\Domain\Kafka\Messages\Listen\Dtos;

use App\Domain\Kafka\Messages\Listen\Dtos\Factories\UserDtoFactory;

/**
 * Класс-dto для сущности "пользователь"
 * Class UserDto
 *
 * @property int $user_id - id пользователя
 * @property string $phone - номер телефона
 */
class UserDto extends AbstractDto
{
    public static function factory(): UserDtoFactory
    {
        return UserDtoFactory::new();
    }
}
