<?php

namespace App\Domain\Kafka\Messages\Send;

use App\Domain\Users\Models\User;
use Ensi\CustomersClient\Dto\Customer;
use Exception;

class PasswordResetFirstEventMessage extends KafkaMessage
{
    public const PHONE = 'phone';
    public const EMAIL = 'email';

    public function __construct(private User $user, private Customer $customer, private string $event)
    {
    }

    public function toArray(): array
    {
        return match ($this->event) {
            self::PHONE => [
                'user_full_name' => $this->customer->getFirstName() . ' ' . $this->customer->getLastName(),
                'user_phone' => $this->customer->getPhone(),
                'user_email' => null,
                'confirmation_code' => $this->user->confirmation_code,
            ],
            self::EMAIL => [
                'user_full_name' => $this->customer->getFirstName() . ' ' . $this->customer->getLastName(),
                'user_email' => $this->customer->getEmail(),
                'user_phone' => null,
                'token' => $this->user->password_token,
            ],
            default => throw new Exception("Неизвестный тип события"),
        };
    }

    public function topicKey(): string
    {
        return 'password-reset-first';
    }
}
