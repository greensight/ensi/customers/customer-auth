<?php

namespace App\Http\ApiV1\Modules\Users\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use App\Rules\PhoneRule;

class CreateConfirmationCodeByPhoneRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'phone' => ['required', 'string', new PhoneRule()],
        ];
    }

    public function phone(): string
    {
        return $this->validated('phone');
    }
}
