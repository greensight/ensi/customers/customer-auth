<?php

namespace App\Domain\Users\Actions\Data;

use Carbon\CarbonInterface;
use Illuminate\Support\Fluent;

/**
 * Покупатель - профиль пользователя
 * Class CustomerDto
 * @package App\Domain\Customers\Actions\Dtos
 *
 * @property int $user_id - id пользователя в сервисе авторизации
 * @property int $status_id - id статуса пользователя
 * @property int $manager_id - id персонального менеджера
 * @property string $yandex_metric_id - Яндекс.Метрика (UserID)
 * @property string $google_analytics_id - Google Analytics (User ID)
 *
 * @property bool $active - Активность пользователя
 * @property string $email - Почта пользователя
 * @property string $phone - Номер телефона пользователя
 * @property string $first_name - Имя
 * @property string $last_name - Фамилия
 * @property string $middle_name - Отчество
 * @property int $gender - Пол
 * @property bool $create_by_admin - Пользователь создан администратором
 * @property string $avatar - Объект с данными об аватаре
 * @property string $city - Город
 * @property CarbonInterface $birthday - День рождения
 * @property CarbonInterface $last_visit_date - Дата последнего визита
 * @property string $comment_status - Комментарий к статусу
 * @property string $timezone
 * @property string $new_email
 * @property string $email_token
 * @property CarbonInterface $email_token_created_at
 */
class CustomerData extends Fluent
{
}
