<?php

namespace App\Domain\Users\Actions\Users;

use App\Domain\Users\Models\User;

class DeactivateConfirmationCodeAction
{
    public function execute(): void
    {
        $usersWithOldConfirmationCodes = User::query()
            ->where('password_token_created_at', '<', now()->subDays(User::LIFE_TIME_CONFIRMATION_CODE))
            ->get();
        if (!$usersWithOldConfirmationCodes->isEmpty()) {
            $usersWithOldConfirmationCodes->each(function (User $user) {
                $user->destroyConfirmCode();
                $user->save();
            });
        }
    }
}
