<?php

namespace App\Http\ApiV1\Modules\Users\Controllers;

use App\Domain\Users\Actions\ConfirmationCodes\CreateConfirmationCodeAction;
use App\Http\ApiV1\Modules\Users\Requests\CreateConfirmationCodeByPhoneRequest;
use App\Http\ApiV1\Modules\Users\Resources\ConfirmationCodesResource;
use Illuminate\Contracts\Support\Responsable;

class ConfirmationCodesController
{
    public function createByPhone(CreateConfirmationCodeByPhoneRequest $request, CreateConfirmationCodeAction $action): Responsable
    {
        return new ConfirmationCodesResource($action->execute($request->phone()));
    }
}
