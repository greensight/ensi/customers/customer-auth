<?php

namespace App\Http\ApiV1\Modules\Users\Resources;

use App\Domain\Users\Actions\ConfirmationCodes\Data\ConfirmationCodeData;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin ConfirmationCodeData
 */
class ConfirmationCodesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'code' => $this->code,
            'message' => $this->message,
            'seconds' => $this->seconds,
            'is_exist' => $this->isUserExist,
        ];
    }
}
