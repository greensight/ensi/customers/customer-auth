<?php

namespace App\Domain\Kafka\Actions\Send;

use App\Domain\Kafka\Messages\Send\PasswordResetFirstEventMessage;
use App\Domain\Users\Models\User;
use Ensi\CustomersClient\Dto\Customer;

class SendPasswordResetFirstEventAction
{
    public function __construct(protected readonly SendKafkaMessageAction $sendAction)
    {
    }

    public function execute(User $user, Customer $customer, string $event): void
    {
        $event = new PasswordResetFirstEventMessage($user, $customer, $event);
        $this->sendAction->execute($event);
    }
}
