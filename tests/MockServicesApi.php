<?php

namespace Tests;

use Ensi\CustomersClient\Api\CustomersApi;
use Mockery\MockInterface;

trait MockServicesApi
{
    // =============== Customers ===============

    // region service Customers
    protected function mockCustomersCustomersApi(): MockInterface|CustomersApi
    {
        return $this->mock(CustomersApi::class);
    }
    // endregion
}
