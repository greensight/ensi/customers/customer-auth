<?php

namespace App\Domain\Users\Observers;

use App\Domain\Kafka\Actions\Send\SendConfirmationCodeEventAction;
use App\Domain\Users\Models\ConfirmationCode;

class ConfirmationCodeAfterCommitObserver
{
    public bool $afterCommit = true;

    public function __construct(
        protected SendConfirmationCodeEventAction $sendConfirmationCodeEventAction,
    ) {
    }

    public function created(ConfirmationCode $model): void
    {
        $this->sendConfirmationCodeEventAction->execute($model);
    }
}
