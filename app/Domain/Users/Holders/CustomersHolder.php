<?php

namespace App\Domain\Users\Holders;

use Ensi\CustomersClient\Api\CustomersApi;
use Ensi\CustomersClient\ApiException;
use Ensi\CustomersClient\Dto\Customer;
use Ensi\CustomersClient\Dto\SearchCustomersRequest;

class CustomersHolder
{
    /** @var array <string, ?Customer> */
    protected array $customers = [];

    public function __construct(protected CustomersApi $customersApi)
    {
    }

    public function getByPhone(string $phone): ?Customer
    {
        if (!array_key_exists($phone, $this->customers)) {
            $this->customers[$phone] = $this->loadCustomer($phone);
        }

        return $this->customers[$phone];
    }

    private function loadCustomer(string $phone): ?Customer
    {
        $request = new SearchCustomersRequest();
        $request->setFilter((object)['phone' => $phone]);

        try {
            return $this->customersApi->searchCustomer($request)->getData();
        } catch (ApiException $e) {
            if ($e->getCode() != 404) {
                throw $e;
            }
        }

        return null;
    }
}
