<?php

namespace App\Domain\Users\Actions\ConfirmationCodes\Data;

use App\Domain\Users\Models\ConfirmationCode;

class ConfirmationCodeData
{
    public function __construct(
        public ?ConfirmationCode $confirmationCode = null,
        public bool $isUserBlocked = false,
        public bool $isUserExist = false,
        public ?string $code = null,
        public ?string $message = null,
        public ?int $seconds = null
    ) {
    }

    public static function timeBlock(int $seconds, bool $isUserBlocked, bool $isUserExist, bool $isUserActive): self
    {
        return new static(
            isUserBlocked: $isUserBlocked,
            isUserExist: $isUserExist,
            code: !$isUserActive ? 'BLOCKED' : ($isUserBlocked ? 'DAY_BLOCK' : 'TIME_BLOCK'),
            message: !$isUserActive ? 'Пользователь заблокирован' : (
                $isUserBlocked
                    ? 'Пользователь заблокирован до следующего дня'
                    : 'Пользователь заблокирован на определенное кол-во секунд'
            ),
            seconds: $seconds,
        );
    }

    public static function code(ConfirmationCode $confirmationCode, int $seconds, bool $isUserExist): self
    {
        return new static(
            confirmationCode: $confirmationCode,
            isUserExist: $isUserExist,
            seconds: $seconds,
        );
    }
}
