<?php

namespace App\Domain\Kafka\Messages\Send;

use App\Domain\Users\Models\ConfirmationCode;

class SendConfirmationCodeEventMessage extends KafkaMessage
{
    public function __construct(private ConfirmationCode $confirmationCode)
    {
    }

    public function toArray(): array
    {
        return [
            'user_id' => $this->confirmationCode->user_id,
            'value' => $this->confirmationCode->value,
        ];
    }

    public function topicKey(): string
    {
        return 'send-confirmation-code';
    }
}
