<?php

namespace App\Domain\Users\Observers;

use App\Domain\Kafka\Actions\Send\SendUserUpdatedEventAction;
use App\Domain\Users\Actions\ConfirmationCodes\DeleteConfirmationCodesAction;
use App\Domain\Users\Models\User;
use App\Exceptions\IllegalOperationException;

class UserObserver
{
    public function __construct(
        private SendUserUpdatedEventAction $sendUserUpdatedEventAction,
        private DeleteConfirmationCodesAction $deleteConfirmationCodesAction
    ) {
    }

    public function saving(User $user): void
    {
        if ($user->login === null && $user->active) {
            throw new IllegalOperationException('Логин не может быть null для активного пользователя');
        }
    }

    public function updated(User $user): void
    {
        $this->sendUserUpdatedEventAction->execute($user);
    }

    public function deleted(User $user): void
    {
        $this->deleteConfirmationCodesAction->execute($user->id);
    }
}
