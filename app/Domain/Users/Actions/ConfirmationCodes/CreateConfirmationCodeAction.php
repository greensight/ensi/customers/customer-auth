<?php

namespace App\Domain\Users\Actions\ConfirmationCodes;

use App\Domain\Users\Actions\ConfirmationCodes\Data\ConfirmationCodeData;
use App\Domain\Users\Holders\CustomersHolder;
use App\Domain\Users\Models\ConfirmationCode;
use Ensi\CustomersClient\Dto\Customer;

class CreateConfirmationCodeAction
{
    public function __construct(protected CustomersHolder $customersHolder)
    {
    }

    public function execute(string $phone): ConfirmationCodeData
    {
        $confirmationCode = ConfirmationCode::findByPhone($phone) ?? ConfirmationCode::makeFromPhone($phone);
        $isUserBlocked = $confirmationCode->isBlocked();

        if (empty($confirmationCode->user_id)) {
            $confirmationCode->user_id = $this->getCustomer($phone)?->getUserId();
        }

        $isUserExist = $confirmationCode->user_id !== null;
        $isCustomerActive = $this->getCustomer($phone)?->getActive() ?? false;
        $isUserActive = !$isUserExist || $isCustomerActive;

        if (!$this->validateConfirmationCode($confirmationCode)) {

            return ConfirmationCodeData::timeBlock(
                (int)ceil($confirmationCode->end_at->diffInSeconds(now(), true)),
                $isUserBlocked,
                $isUserExist,
                $isUserActive
            );
        }

        $confirmationCode->generateCode()->save();

        return ConfirmationCodeData::code($confirmationCode, (int)ceil($confirmationCode->end_at->diffInSeconds(now(), true)), $isUserExist);
    }

    private function validateConfirmationCode(ConfirmationCode $confirmationCode): bool
    {
        return $confirmationCode->end_at === null || $confirmationCode->end_at->isPast();
    }

    private function getCustomer(string $phone): ?Customer
    {
        return $this->customersHolder->getByPhone($phone);
    }
}
