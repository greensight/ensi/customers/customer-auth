<?php

namespace App\Providers;

use App\Domain\Users\Models\ConfirmationCode;
use App\Domain\Users\Models\User;
use App\Domain\Users\Observers\ConfirmationCodeAfterCommitObserver;
use App\Domain\Users\Observers\UserObserver;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array<class-string, array<int, class-string>>
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        \SocialiteProviders\Manager\SocialiteWasCalled::class => [
            // add your listeners (aka providers) here
            'SocialiteProviders\\VKontakte\\VKontakteExtendSocialite@handle',
            'SocialiteProviders\\Facebook\\FacebookExtendSocialite@handle',
        ],
    ];

    public function boot(): void
    {
        User::observe(UserObserver::class);
        ConfirmationCode::observe(ConfirmationCodeAfterCommitObserver::class);
    }

    public function shouldDiscoverEvents(): bool
    {
        return false;
    }
}
