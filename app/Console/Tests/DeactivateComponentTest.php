<?php

use function Pest\Laravel\artisan;

use Tests\ComponentTestCase;

uses(ComponentTestCase::class);
uses()->group('component');

test("Command passwords:deactivate-token success", function () {
    artisan("passwords:deactivate-token")->assertSuccessful();
});
