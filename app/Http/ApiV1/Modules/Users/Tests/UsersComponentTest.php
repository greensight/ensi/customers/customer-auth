<?php

use App\Domain\Customers\Actions\CreateCustomerAction;
use App\Domain\Customers\Tests\Factories\CustomerFactory;
use App\Domain\Users\Models\ConfirmationCode;
use App\Domain\Users\Models\User;
use App\Exceptions\AuthUserException;
use App\Http\ApiV1\Modules\OAuth\Tests\Factories\RegisterRequestFactory;
use App\Http\ApiV1\Modules\Users\Tests\Factories\UserRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Illuminate\Support\Collection;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertDatabaseMissing;
use function Pest\Laravel\deleteJson;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('POST /api/v1/users:register success with password', function () {
    /** @var ApiV1ComponentTestCase $this */
    $registerRequest = RegisterRequestFactory::new()->withPassword()->make();

    $this->mock(CreateCustomerAction::class)->shouldReceive('execute');
    $this->mockCustomersCustomersApi()
        ->shouldReceive('searchCustomers->getData')
        ->times(2) // 1 - поиск по почте, 2 - поиск по телефону
        ->andReturn([]);

    postJson('/api/v1/users:register', $registerRequest)
        ->assertOk();

    assertDatabaseHas(User::class, [
        'login' => $registerRequest['login'],
        'active' => true,
    ]);
});

test('POST /api/v1/users:register success with code', function () {
    /** @var ApiV1ComponentTestCase $this */
    /** @var ConfirmationCode $confirmationCode */
    $confirmationCode = ConfirmationCode::factory()->forRegistration()->createQuietly();
    $registerRequest = RegisterRequestFactory::new()->withCode($confirmationCode->value, $confirmationCode->phone)->make();

    $this->mock(CreateCustomerAction::class)->shouldReceive('execute');
    $this->mockCustomersCustomersApi()
        ->shouldReceive('searchCustomers->getData')
        ->times(2) // 1 - поиск по почте, 2 - поиск по телефону
        ->andReturn([]);

    postJson('/api/v1/users:register', $registerRequest)
        ->assertOk();

    assertDatabaseHas(User::class, [
        'login' => $registerRequest['login'],
        'active' => true,
    ]);
    assertDatabaseMissing(ConfirmationCode::class, [
        'phone' => $registerRequest['phone'],
    ]);
});

test('POST /api/v1/users:register fail on duplicate email on customer', function () {
    /** @var ApiV1ComponentTestCase $this */
    $registerRequest = RegisterRequestFactory::new()->withPassword()->make();

    $this->mockCustomersCustomersApi()
        ->shouldReceive('searchCustomers->getData')
        ->once()
        ->andReturn(CustomerFactory::new()->make()->toArray());

    postJson('/api/v1/users:register', $registerRequest)
        ->assertBadRequest()
        ->assertJsonPath('errors.0.message', AuthUserException::emailIsBusyUser()->getMessage())
        ->assertJsonPath('errors.0.code', AuthUserException::emailIsBusyUser()->getTextCode());

    assertDatabaseMissing(User::class, ['login' => $registerRequest['login']]);
});

test('POST /api/v1/users:register fail on duplicate email on user', function () {
    /** @var ApiV1ComponentTestCase $this */
    $registerRequest = RegisterRequestFactory::new()->withPassword()->make();

    User::factory()->create(['login' => $registerRequest['login']]);

    postJson('/api/v1/users:register', $registerRequest)
        ->assertBadRequest()
        ->assertJsonPath('errors.0.message', AuthUserException::emailIsBusyUser()->getMessage())
        ->assertJsonPath('errors.0.code', AuthUserException::emailIsBusyUser()->getTextCode());
});

test('POST /api/v1/users:register expired code', function () {
    /** @var ApiV1ComponentTestCase $this */
    /** @var ConfirmationCode $confirmationCode */
    $confirmationCode = ConfirmationCode::factory()->forRegistration()->expired()->createQuietly();
    $registerRequest = RegisterRequestFactory::new()->withCode($confirmationCode->value, $confirmationCode->phone)->make();

    postJson('/api/v1/users:register', $registerRequest)
        ->assertBadRequest()
        ->assertJsonPath('errors.0.message', AuthUserException::codeExpired()->getMessage())
        ->assertJsonPath('errors.0.code', AuthUserException::codeExpired()->getTextCode());

    assertDatabaseMissing(User::class, ['login' => $registerRequest['login']]);
    assertDatabaseHas(ConfirmationCode::class, ['phone' => $registerRequest['phone']]);
});

// region users:search
test("POST /api/v1/users:search success", function () {
    /** @var Collection|User[] $users */
    $users = User::factory()->count(3)->create();
    $filterId = $users->last()->id;

    postJson("/api/v1/users:search", [
        "filter" => ["id" => $filterId],
        "sort" => ["id"],
    ])
        ->assertOk()
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $filterId);
});

test("POST /api/v1/users:search sort success", function (string $sort) {
    User::factory()->create();
    postJson("/api/v1/users:search", ["sort" => ["$sort"]])->assertOk();
})->with(['id', 'created_at', 'updated_at']);

test("POST /api/v1/users:search filter success", function (
    string $fieldKey,
    $value = null,
    ?string $filterKey = null,
    $filterValue = null
) {
    /** @var User $order */
    $order = User::factory()->create($value ? [$fieldKey => $value] : []);
    postJson("/api/v1/users:search", [
        "filter" => [
            ($filterKey ?: $fieldKey) => ($filterValue ?: $order->{$fieldKey}),
        ],
    ])
        ->assertOk()
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $order->id);
})->with([
    ['login', 'test_login_check', 'login', ['test_login_check', 'test_login_check_one']],
    ['login', 'test_login_check'],
    ['login', 'test_login_check', 'login_like', 't_login'],
    ['active', false],
    ['id', 4, 'id', [4, 5, 6]],
    ['id', 4],
]);
// endregion

// region users:search-one
test("POST /api/v1/users:search-one success", function () {
    $users = User::factory()
        ->count(10)
        ->create();
    /** @var User $requiredUser */
    $requiredUser = $users->last();

    postJson("/api/v1/users:search-one", [
        "filter" => ["id" => $requiredUser->id],
        "sort" => ["id"],
    ])
        ->assertOk()
        ->assertJsonPath('data.id', $requiredUser->id)
        ->assertJsonPath('data.login', $requiredUser->login)
        ->assertJsonPath('data.active', $requiredUser->active);
});
// endregion

// region users/{id}
test("GET /api/v1/users/{id} success", function () {
    /** @var User $user */
    $user = User::factory()->create();

    getJson("/api/v1/users/{$user->id}")
        ->assertOk()
        ->assertJsonPath('data.id', $user->id)
        ->assertJsonPath('data.login', $user->login)
        ->assertJsonPath('data.active', $user->active);
});

test("GET /api/v1/users/{id} 404", function () {
    getJson("/api/v1/users/1")
        ->assertNotFound()
        ->assertJsonPath('data', null)
        ->assertJsonPath('errors.0.code', "NotFoundHttpException");
});

test("PATCH /api/v1/users/{id} success", function () {
    /** @var ApiV1ComponentTestCase $this */
    /** @var User $user */
    $user = User::factory()->create();

    $userData = UserRequestFactory::new()->make();

    patchJson("/api/v1/users/{$user->id}", $userData)
        ->assertOk()
        ->assertJsonPath('data.id', $user->id)
        ->assertJsonPath('data.login', $userData['login'])
        ->assertJsonPath('data.active', $userData['active']);
    assertDatabaseHas(User::class, [
        'id' => $user->id,
        'active' => $userData['active'],
        'login' => $userData['login'],
    ]);
});

test("PATCH /api/v1/users/{id} 400 same login", function () {
    /** @var User $userOther */
    $userOther = User::factory()->create();
    /** @var User $user */
    $user = User::factory()->create();
    $userData = UserRequestFactory::new()->only(['login'])->make(['login' => $userOther->login]);

    patchJson("/api/v1/users/{$user->id}", $userData)
        ->assertStatus(400)
        ->assertJsonPath('data', null)
        ->assertJsonPath('errors.0.code', "ValidationError");
});

test("PATCH /api/v1/users/{id} 404", function () {
    patchJson("/api/v1/users/1", UserRequestFactory::new()->make())
        ->assertNotFound()
        ->assertJsonPath('data', null)
        ->assertJsonPath('errors.0.code', "NotFoundHttpException");
});

test("DELETE /api/v1/users/{id} success", function () {
    /** @var User $user */
    $user = User::factory()->create();

    deleteJson("/api/v1/users/{$user->id}")->assertOk();
    assertDatabaseMissing(User::class, [
        'id' => $user->id,
        'active' => $user->active,
        'login' => $user->login,
    ]);
});
// endregion

// region users
test("POST /api/v1/users success", function () {
    $userData = UserRequestFactory::new()->make();
    postJson("/api/v1/users", $userData)
        ->assertCreated()
        ->assertJsonPath('data.active', $userData['active'])
        ->assertJsonPath('data.login', $userData['login']);
    assertDatabaseHas(User::class, [
        'active' => $userData['active'],
        'login' => $userData['login'],
    ]);
});

test("POST /api/v1/users 400 same login", function () {
    /** @var User $user */
    $user = User::factory()->create();
    $newUserData = UserRequestFactory::new()->make();
    $newUserData['login'] = $user->login;
    postJson("/api/v1/users", $newUserData)
        ->assertStatus(400)
        ->assertJsonPath('errors.0.code', "ValidationError");
});
// endregion

// region users/{id}:refresh-password-token
//test("POST /api/v1/users/{id}:refresh-password-token success", function () {
//    /** @var User $user */
//    $user = User::factory()->create();
//
//    postJson("/api/v1/users/{$user->id}:refresh-password-token")
//        ->assertOk();
//});
test("POST /api/v1/users/{id}:refresh-password-token 404", function () {
    postJson("/api/v1/users/1:refresh-password-token")->assertNotFound();
});
// endregion
