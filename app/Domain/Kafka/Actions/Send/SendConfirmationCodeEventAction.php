<?php

namespace App\Domain\Kafka\Actions\Send;

use App\Domain\Kafka\Messages\Send\SendConfirmationCodeEventMessage;
use App\Domain\Users\Models\ConfirmationCode;

class SendConfirmationCodeEventAction
{
    public function __construct(protected readonly SendKafkaMessageAction $sendAction)
    {
    }

    public function execute(ConfirmationCode $confirmationCode): void
    {
        $event = new SendConfirmationCodeEventMessage($confirmationCode);
        $this->sendAction->execute($event);
    }
}
