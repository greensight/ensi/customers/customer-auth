<?php

namespace App\Domain\Kafka\Messages\Listen\Tests\Factories;

use App\Domain\Kafka\Messages\Listen\Dtos\Factories\UserDtoFactory;
use App\Http\ApiV1\Support\Tests\Factories\KafkaMessageFactory;

class UserPhoneChangeMessageFactory extends KafkaMessageFactory
{
    protected function definition(): array
    {
        return array_merge(parent::definition(), [
            'attributes' => UserDtoFactory::new()->make()->toArray(),
        ]);
    }
}
