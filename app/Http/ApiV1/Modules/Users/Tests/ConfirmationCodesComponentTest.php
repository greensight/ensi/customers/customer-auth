<?php

use App\Domain\Customers\Tests\Factories\CustomerFactory;
use App\Domain\Kafka\Actions\Send\SendConfirmationCodeEventAction;
use App\Domain\Users\Models\ConfirmationCode;
use App\Domain\Users\Models\User;
use App\Http\ApiV1\Modules\Users\Tests\Factories\ConfirmationCodeRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\CustomersClient\ApiException;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'confirmation-code');

test('POST /api/v1/users/confirmation-code-by-phone 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    /** @var User $user */
    $user = User::factory()->create();

    $request = ConfirmationCodeRequestFactory::new()->make();

    $this->mock(SendConfirmationCodeEventAction::class)
        ->shouldReceive('execute')
        ->once();

    $this->mockCustomersCustomersApi()
        ->shouldReceive('searchCustomer->getData')
        ->once()
        ->andReturn(CustomerFactory::new()->make(['user_id' => $user->id]));

    $seconds = postJson('/api/v1/users/confirmation-code-by-phone', $request)
        ->assertOk()
        ->assertJsonPath('data.message', null)
        ->json('data.seconds');

    assert(0 < $seconds && $seconds <= config('app.confirmation_code.failed_attempt_1'));
});

test("POST /api/v1/users/confirmation-code-by-phone 200 if don't delete user data", function () {
    /** @var ApiV1ComponentTestCase $this */
    $phone = '+77777777777';
    /** @var User $user */
    $user = User::factory()->create();
    ConfirmationCode::factory()->create(['user_id' => $user->id, 'phone' => $phone]);

    $request = ConfirmationCodeRequestFactory::new()->make(['phone' => $phone]);

    $this->mockCustomersCustomersApi()
        ->shouldReceive('searchCustomer->getData')
        ->once()
        ->andThrow(new ApiException('', 404));

    postJson('/api/v1/users/confirmation-code-by-phone', $request)
        ->assertJsonPath('data.message', null)
        ->assertOk();
});

test('POST /api/v1/users/confirmation-code-by-phone time block 400', function () {
    /** @var ApiV1ComponentTestCase $this */
    /** @var ConfirmationCode $confirmationCode */
    $confirmationCode = ConfirmationCode::factory()->forRegistration()->expired()->createQuietly();

    $request = ConfirmationCodeRequestFactory::new()->make(['phone' => $confirmationCode->phone]);

    $this->mockCustomersCustomersApi()
        ->shouldReceive('searchCustomer->getData')
        ->once()
        ->andThrow(new ApiException('', 404));

    postJson('/api/v1/users/confirmation-code-by-phone', $request)
        ->assertJsonPath('data.code', 'TIME_BLOCK')
        ->assertOk();
});

test('POST /api/v1/users/confirmation-code-by-phone after block 200', function (int $currentAttempt, int $newAttempt) {
    /** @var ApiV1ComponentTestCase $this */
    /** @var ConfirmationCode $confirmationCode */
    $confirmationCode = ConfirmationCode::factory()->blocked($currentAttempt)->createQuietly();

    $request = ConfirmationCodeRequestFactory::new()->make(['phone' => $confirmationCode->phone]);

    $this->mockCustomersCustomersApi()
        ->shouldReceive('searchCustomer->getData')
        ->once()
        ->andReturn(CustomerFactory::new()->make(['user_id' => $confirmationCode->user_id]));

    postJson('/api/v1/users/confirmation-code-by-phone', $request)
        ->assertOk();

    assertDatabaseHas(ConfirmationCode::class, [
        'id' => $confirmationCode->id,
        'attempt' => $newAttempt,
    ]);
})->with([
    [1, 2],
    [2, 3],
    [3, 1],
]);
