<?php

use App\Domain\Customers\Tests\Factories\CustomerFactory;
use App\Domain\Users\Models\User;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LaravelTestFactories\FakerProvider;

use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test("POST /api/v1/users/password-reset-first (by email)", function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var ApiV1ComponentTestCase $this */

    /** @var User $user */
    $user = User::factory()->create();

    $customerFields = CustomerFactory::new()->make(['user_id' => $user->id]);
    $customerFieldsSearch = CustomerFactory::new()->makeResponse(['user_id' => $user->id]);
    $this->mockCustomersCustomersApi()->shouldReceive('createCustomer')
        ->andReturn($customerFields);

    $requestBodyForFirstStep = [
        "email" => $customerFields->getEmail(),
    ];

    $this->mockCustomersCustomersApi()->shouldReceive('searchCustomer')
        ->andReturn($customerFieldsSearch);

    postJson('/api/v1/users/password-reset-first', $requestBodyForFirstStep)->assertStatus(200);
})->with(FakerProvider::$optionalDataset);

test("POST /api/v1/users/password-reset-second (by email)", function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var ApiV1ComponentTestCase $this */

    /** @var User $user */
    $user = User::factory()->create();

    $customer = CustomerFactory::new()->make(['user_id' => $user->id]);
    $customerFieldsSearch = CustomerFactory::new()->makeResponse(['user_id' => $user->id]);
    $this->mockCustomersCustomersApi()->shouldReceive('createCustomer')
        ->andReturn($customer);

    $this->mockCustomersCustomersApi()->shouldReceive('searchCustomer')
        ->andReturn($customerFieldsSearch);

    $requestBodyForSecondStep = [
        'reset_type' => 'by_token',
        'code' => $user->password_token,
        'email' => $customer->getEmail(),
        'password' => 'newTestPassword',
    ];
    postJson('/api/v1/users/password-reset-second', $requestBodyForSecondStep)->assertStatus(200);
})->with(FakerProvider::$optionalDataset);

test("POST /api/v1/users/password-reset-first (by phone)", function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var ApiV1ComponentTestCase $this */

    /** @var User $user */
    $user = User::factory()->create();

    $customerFields = CustomerFactory::new()->make(['user_id' => $user->id]);
    $customerFieldsSearch = CustomerFactory::new()->makeResponse(['user_id' => $user->id]);
    $this->mockCustomersCustomersApi()->shouldReceive('createCustomer')
        ->andReturn($customerFields);

    $requestBodyForFirstStep = [
        "phone" => $customerFields->getPhone(),
    ];

    $this->mockCustomersCustomersApi()->shouldReceive('searchCustomer')
        ->andReturn($customerFieldsSearch);

    postJson('/api/v1/users/password-reset-first', $requestBodyForFirstStep)->assertStatus(200);
})->with(FakerProvider::$optionalDataset);

test("POST /api/v1/users/password-reset-second (by phone)", function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var ApiV1ComponentTestCase $this */

    /** @var User $user */
    $user = User::factory()->create();

    $customer = CustomerFactory::new()->make(['user_id' => $user->id]);
    $customerFieldsSearch = CustomerFactory::new()->makeResponse(['user_id' => $user->id]);
    $this->mockCustomersCustomersApi()->shouldReceive('createCustomer')
        ->andReturn($customer);


    $this->mockCustomersCustomersApi()->shouldReceive('searchCustomer')
        ->andReturn($customerFieldsSearch);

    $requestBodyForSecondStep = [
        'reset_type' => 'by_phone',
        'code' => $user->confirmation_code,
        'phone' => $customer->getPhone(),
        'password' => 'newTestPassword',
    ];
    postJson('/api/v1/users/password-reset-second', $requestBodyForSecondStep)->assertStatus(200);
})->with(FakerProvider::$optionalDataset);
