<?php

namespace App\Http\ApiV1\Modules\Users\Resources;

use App\Domain\Users\Models\User;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/** @mixin User */
class UsersResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'login' => $this->login,
            'active' => $this->active,
            'confirmation_code' => $this->confirmation_code,
            'password_token' => $this->password_token,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
