<?php

namespace App\Http\ApiV1\Modules\Common\Tests\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;

class DeleteCustomerPersonalDataRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'user_id' => $this->faker->modelId(),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
