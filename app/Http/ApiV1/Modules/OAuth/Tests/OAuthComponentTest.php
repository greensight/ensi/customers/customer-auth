<?php

use App\Domain\Users\Models\ConfirmationCode;
use App\Domain\Users\Models\User;
use App\Exceptions\AuthUserException;
use App\Http\ApiV1\Modules\OAuth\Tests\Factories\CreateTokenRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Laravel\Passport\ClientRepository;
use Laravel\Passport\Token;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\deleteJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component')->beforeEach(function () {
    (new ClientRepository())->createPersonalAccessClient(111, 'Test Personal Access Client', '/');
});

test('POST /api/v1/oauth/token success', function () {
    $password = 'Test1234!';
    /** @var User $user */
    $user = User::factory()->active()->create(['password' => $password]);

    $requestBody = CreateTokenRequestFactory::new()->asPassword($user->login, $password)->make();

    postJson('/api/v1/oauth/token', $requestBody)->assertStatus(200);

    assertDatabaseHas(Token::class, [
        'user_id' => $user->id,
        'client_id' => $requestBody['client_id'],
    ]);
});

test('POST /api/v1/oauth/token by confirmation code success', function () {
    /** @var User $user */
    $user = User::factory()->active()->create();
    /** @var ConfirmationCode $confirmationCode */
    $confirmationCode = ConfirmationCode::factory()->active()->createQuietly(['user_id' => $user->id]);
    $requestBody = CreateTokenRequestFactory::new()
        ->asConfirmationCodeByPhone($confirmationCode->phone, $confirmationCode->value)
        ->make();

    postJson('/api/v1/oauth/token', $requestBody)
        ->assertStatus(200);

    assertDatabaseHas(Token::class, [
        'user_id' => $user->id,
        'client_id' => $requestBody['client_id'],
    ]);
});

test('POST /api/v1/oauth/token incorrect credentials', function () {
    $correctPassword = 'Test1234!';
    $wrongPassword = 'Test1234!*';
    /** @var User $user */
    $user = User::factory()->active()->create(['password' => $correctPassword]);

    $requestBody = CreateTokenRequestFactory::new()->asPassword($user->login, $wrongPassword)->make();

    $this->postJson('/api/v1/oauth/token', $requestBody)
        ->assertStatus(401);
});

test('POST /api/v1/oauth/token incorrect code', function () {
    /** @var User $user */
    $user = User::factory()->active()->create();

    $correctCode = '1234';
    $wrongCode = '1111';

    /** @var ConfirmationCode $confirmationCode */
    $confirmationCode = ConfirmationCode::factory()->active()->createQuietly(['user_id' => $user->id, 'value' => $correctCode]);

    $requestBody = CreateTokenRequestFactory::new()
        ->asConfirmationCodeByPhone($confirmationCode->phone, $wrongCode)
        ->make();

    $this->postJson('/api/v1/oauth/token', $requestBody)
        ->assertBadRequest()
        ->assertJsonPath('errors.0.message', AuthUserException::codeError()->getMessage())
        ->assertJsonPath('errors.0.code', AuthUserException::codeError()->getTextCode());
});

test('POST /api/v1/oauth/token - ConfirmationCodeByPhone - user undefined', function () {
    /** @var ConfirmationCode $confirmationCode */
    $confirmationCode = ConfirmationCode::factory()->active()->createQuietly(['user_id' => null]);

    $requestBody = CreateTokenRequestFactory::new()
        ->asConfirmationCodeByPhone($confirmationCode->phone, $confirmationCode->value)
        ->make();

    postJson('/api/v1/oauth/token', $requestBody)
        ->assertBadRequest()
        ->assertJsonPath('errors.0.message', AuthUserException::notFound()->getMessage())
        ->assertJsonPath('errors.0.code', AuthUserException::notFound()->getTextCode());
});

test('POST /api/v1/oauth/token - ConfirmationCodeByPhone - user unactive', function () {
    /** @var User $user */
    $user = User::factory()->active(false)->create();
    /** @var ConfirmationCode $confirmationCode */
    $confirmationCode = ConfirmationCode::factory()->active()->createQuietly(['user_id' => $user->id]);

    $requestBody = CreateTokenRequestFactory::new()
        ->asConfirmationCodeByPhone($confirmationCode->phone, $confirmationCode->value)
        ->make();

    postJson('/api/v1/oauth/token', $requestBody)
        ->assertBadRequest()
        ->assertJsonPath('errors.0.message', AuthUserException::blocked()->getMessage())
        ->assertJsonPath('errors.0.code', AuthUserException::blocked()->getTextCode());
});

test('DELETE /api/v1/oauth/tokens/{id} success', function () {
    /** @var User $user */
    $user = User::factory()->create();

    $tokenRecord = $user->createToken('test');
    $tokenId = $tokenRecord->token->getAttribute('id');

    $this->skipNextOpenApiRequestValidation();

    deleteJson("/api/v1/oauth/tokens/{$tokenId}", [], ['Authorization' => 'Bearer ' . $tokenRecord->accessToken])
        ->assertStatus(204);

    assertDatabaseHas(Token::class, [
        'id' => $tokenId,
        'revoked' => true,
    ]);
});

test('DELETE /api/v1/oauth/tokens/{id} 404', function () {
    /** @var User $user */
    $user = User::factory()->create();

    $tokenRecord = $user->createToken('test');
    $wrongTokenId = 'wrong_token';

    $this->skipNextOpenApiRequestValidation();

    deleteJson("/api/v1/oauth/tokens/$wrongTokenId", [], ['Authorization' => 'Bearer ' . $tokenRecord->accessToken])
        ->assertStatus(404);

    assertDatabaseHas(Token::class, [
        'id' => $tokenRecord->token->getAttribute('id'),
        'revoked' => false,
    ]);
});
