<?php

namespace App\Domain\Users\Actions\Users;

use App\Domain\Users\Models\User;

class DeleteUserAction
{
    public function execute(int $id): void
    {
        User::destroy($id);
    }
}
