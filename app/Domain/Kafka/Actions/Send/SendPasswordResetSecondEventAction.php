<?php

namespace App\Domain\Kafka\Actions\Send;

use App\Domain\Kafka\Messages\Send\PasswordResetSecondEventMessage;
use Ensi\CustomersClient\Dto\Customer;

class SendPasswordResetSecondEventAction
{
    public function __construct(protected readonly SendKafkaMessageAction $sendAction)
    {
    }

    public function execute(Customer $customer): void
    {
        $event = new PasswordResetSecondEventMessage($customer);
        $this->sendAction->execute($event);
    }
}
