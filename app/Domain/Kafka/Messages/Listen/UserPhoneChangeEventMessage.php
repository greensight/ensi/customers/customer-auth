<?php

namespace App\Domain\Kafka\Messages\Listen;

use App\Domain\Kafka\Messages\Listen\Dtos\UserDto;

/**
 * @property UserDto $attributes
 */
class UserPhoneChangeEventMessage extends AbstractEventMessage
{
    protected function getDtoClass(): string
    {
        return UserDto::class;
    }
}
