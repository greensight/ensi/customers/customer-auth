<?php

namespace App\Domain\Customers\Actions;

use Ensi\CustomersClient\Api\CustomersApi;
use Ensi\CustomersClient\ApiException;
use Ensi\CustomersClient\Dto\CreateCustomerRequest;
use Ensi\CustomersClient\Dto\Customer;

class CreateCustomerAction
{
    public function __construct(protected CustomersApi $customersApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(array $fields): Customer
    {
        return $this->customersApi->createCustomer(new CreateCustomerRequest($fields))->getData();
    }
}
