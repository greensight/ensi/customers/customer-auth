<?php

use App\Domain\Users\Models\User;
use Illuminate\Database\QueryException;
use Illuminate\Support\Collection;

use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertNotNull;
use function PHPUnit\Framework\assertNull;
use function PHPUnit\Framework\assertTrue;

use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class);
uses()->group('unit');

test("User create success", function () {
    $user = User::factory()->create();
    assertNotNull($user);
});
test("User check password", function () {
    $password = 'testpassword123';
    /** @var User $user */
    $user = User::factory()->create(['password' => $password]);
    assertTrue($user->checkPassword($password));
    assertTrue($user->validateForPassportPasswordGrant($password));
});
test("User check findForPassport", function () {
    /** @var User $user */
    $user = User::factory()->create();
    assertEquals($user->findForPassport($user->login)->first()->id, $user->id);
});
test("User generate and destroy password token", function () {
    /** @var User $user */
    $user = User::factory()->create();
    $user->generatePasswordToken();
    assertNotNull($user->password_token);
    $user->destroyPasswordToken();
    assertNull($user->password_token);
});
test("User duplicate login", function () {
    /** @var Collection|User[] $users */
    $users = User::factory()->count(3)->create();
    $users->first()->update(['login' => $users->last()->login]);
})->throws(QueryException::class);
