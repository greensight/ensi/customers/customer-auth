<?php

use App\Domain\Users\Models\ConfirmationCode;
use App\Domain\Users\Models\User;
use App\Http\ApiV1\Modules\Common\Tests\Factories\DeleteCustomerPersonalDataRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertModelExists;
use function Pest\Laravel\assertModelMissing;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('POST /api/v1/common/customer-personal-data:delete 200', function () {
    /** @var User $user */
    $user = User::factory()->withPersonalData()->create();

    /** @var ConfirmationCode $confirmationCode */
    $confirmationCode = ConfirmationCode::factory()->create(['user_id' => $user->id]);
    /** @var ConfirmationCode $otherConfirmationCode */
    $otherConfirmationCode = ConfirmationCode::factory()->create();

    $request = DeleteCustomerPersonalDataRequestFactory::new()->make(['user_id' => $user->id]);


    postJson('/api/v1/common/customer-personal-data:delete', $request)
        ->assertStatus(200);

    assertDatabaseHas(User::class, [
        'id' => $user->id,
        'login' => null,
        'password' => null,
        'active' => false,
    ]);

    assertModelMissing($confirmationCode);
    assertModelExists($otherConfirmationCode);
});
