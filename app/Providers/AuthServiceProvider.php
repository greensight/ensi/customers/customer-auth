<?php

namespace App\Providers;

use App\Domain\Common\Grants\ConfirmationCodeGrant;
use App\Domain\Users\Actions\ConfirmationCodes\VerifyConfirmationCodeAction;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Laravel\Passport\Bridge\RefreshTokenRepository;
use Laravel\Passport\Passport;
use League\OAuth2\Server\AuthorizationServer;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    public function boot(): void
    {
        Passport::$ignoreCsrfToken = true;

        Passport::enablePasswordGrant();
        Passport::tokensExpireIn(now()->addDays(1));
        Passport::refreshTokensExpireIn(now()->addDays(60));
        Passport::personalAccessTokensExpireIn(now()->addMonths(6));
        Passport::loadKeysFrom(config('passport.keys_path'));

        $this->app->afterResolving(AuthorizationServer::class, function (AuthorizationServer $authorizationServer) {
            $authorizationServer->enableGrantType(
                $this->makeConfirmationCodeGrant(),
                Passport::tokensExpireIn()
            );
        });
    }

    protected function makeConfirmationCodeGrant(): ConfirmationCodeGrant
    {
        $grant = new ConfirmationCodeGrant(
            $this->app->make(RefreshTokenRepository::class),
            $this->app->make(VerifyConfirmationCodeAction::class)
        );

        $grant->setRefreshTokenTTL(Passport::refreshTokensExpireIn());

        return $grant;
    }
}
