<?php

namespace App\Domain\Users\Actions\Users;

use App\Domain\Kafka\Actions\Send\SendPasswordResetSecondEventAction;
use App\Domain\Users\Models\User;
use Ensi\CustomersClient\Api\CustomersApi;
use Ensi\CustomersClient\Dto\Customer;
use Ensi\CustomersClient\Dto\SearchCustomersRequest;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class PasswordResetSecondStepUserAction
{
    public function __construct(
        protected CustomersApi $customersApi,
        protected SendPasswordResetSecondEventAction $passwordResetSecondEventAction,
    ) {
    }

    public function execute(array $fields): User
    {
        if ($fields['reset_type'] == 'by_phone') {
            $user = $this->resetByPhone($fields);
        } else {
            $user = $this->resetByEmail($fields);
        }

        return $user;
    }

    private function resetByPhone(array $fields): User
    {
        if (!isset($fields['phone'])) {
            throw new BadRequestHttpException('Phone is required');
        }
        $request = new SearchCustomersRequest([
            'filter' => [
                'phone' => $fields['phone'],
            ],
        ]);
        $customer = $this->customersApi->searchCustomer($request)->getData();

        /** @var User $user */
        $user = User::query()->findOrFail($customer->getUserId());
        if (!$user->checkConfirmCode($fields['code'])) {
            throw new BadRequestHttpException('Недействительный проверочный код.');
        }

        return $this->updateUserAndSendMessage($user, $customer, $fields, true);
    }

    private function resetByEmail(array $fields): User
    {
        if (!isset($fields['email'])) {
            throw new BadRequestHttpException('Email is required');
        }
        $request = new SearchCustomersRequest([
            'filter' => [
                'email' => $fields['email'],
            ],
        ]);
        $customer = $this->customersApi->searchCustomer($request)->getData();

        /** @var User|null $user */
        $user = User::findByPasswordToken($fields['code']);
        if (empty($user)) {
            throw new BadRequestHttpException('Ссылка установки пароля недействительна. Для генерации новой обратитесь к администратору');
        }

        return $this->updateUserAndSendMessage($user, $customer, $fields, false);
    }

    private function updateUserAndSendMessage(User $user, Customer $customer, array $fields, bool $byPhone): User
    {
        $user->setPasswordAttribute($fields['password']);
        $byPhone ? $user->destroyConfirmCode() : $user->destroyPasswordToken();
        $user->save();

        $this->passwordResetSecondEventAction->execute($customer);

        return $user;
    }
}
