<?php

namespace App\Http\ApiV1\Modules\Users\Requests;

use App\Domain\Users\Models\User;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class PatchUserRequest extends BaseFormRequest
{
    public function rules(): array
    {
        $id = (int)$this->route('id');

        return [
            'login' => [Rule::unique(User::class)->ignore($id)],
            'active' => ['boolean'],
            'password' => ['nullable'],
        ];
    }
}
