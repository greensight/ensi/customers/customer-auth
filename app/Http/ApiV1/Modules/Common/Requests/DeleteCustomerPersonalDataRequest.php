<?php

namespace App\Http\ApiV1\Modules\Common\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class DeleteCustomerPersonalDataRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'user_id' => ['required', 'integer'],
        ];
    }

    public function getUserId(): int
    {
        return $this->input('user_id');
    }
}
