<?php

namespace App\Domain\Users\Actions\ConfirmationCodes;

use App\Domain\Users\Models\ConfirmationCode;
use Illuminate\Database\Eloquent\Collection;

class DeleteConfirmationCodesAction
{
    public function execute(int $userId): void
    {
        /** @var Collection|ConfirmationCode[] $confirmationCodes */
        $confirmationCodes = ConfirmationCode::query()
            ->where('user_id', $userId)
            ->get();

        foreach ($confirmationCodes as $confirmationCode) {
            $confirmationCode->delete();
        }
    }
}
