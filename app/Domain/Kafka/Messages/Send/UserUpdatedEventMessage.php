<?php

namespace App\Domain\Kafka\Messages\Send;

use App\Domain\Users\Models\User;

class UserUpdatedEventMessage extends KafkaMessage
{
    public function __construct(private User $user)
    {
    }

    public function toArray(): array
    {
        $dirty = $this->user->getDirty();
        $oldData = [];
        foreach ($dirty as $attributeKey => $attributeValue) {
            switch ($attributeKey) {
                case 'updated_at':
                    break;
                case 'password':
                    $oldData['password'] = '';

                    break;
                default:
                    $oldData[$attributeKey] = $this->user->getOriginal($attributeKey);
            }
        }

        return [
            'user_id' => $this->user->id,
            'old_data' => $oldData,
        ];
    }

    public function topicKey(): string
    {
        return 'user-updated';
    }
}
