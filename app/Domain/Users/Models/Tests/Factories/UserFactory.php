<?php

namespace App\Domain\Users\Models\Tests\Factories;

use App\Domain\Users\Models\User;
use Ensi\LaravelTestFactories\BaseModelFactory;

class UserFactory extends BaseModelFactory
{
    protected $model = User::class;

    public function definition(): array
    {
        return [
            'login' => $this->faker->unique()->userName,
            'password' => $this->faker->password(),
            'active' => $this->faker->boolean(),
            'password_token' => '12345qwe',
            'confirmation_code' => '1234',
        ];
    }

    public function withPersonalData(): static
    {
        return $this->state([
            'login' => $this->faker->unique()->userName,
        ]);
    }

    public function active(bool $active = true): static
    {
        return $this->state([
            'active' => $active,
        ]);
    }
}
