<?php

namespace App\Domain\Common\Grants;

use App\Domain\Users\Actions\ConfirmationCodes\VerifyConfirmationCodeAction;
use App\Domain\Users\Models\ConfirmationCode;
use App\Domain\Users\Models\User;
use App\Exceptions\AuthUserException;
use App\Http\ApiV1\OpenApiGenerated\Enums\GrantTypeEnum;
use DateInterval;
use League\OAuth2\Server\Grant\AbstractGrant;
use League\OAuth2\Server\Repositories\RefreshTokenRepositoryInterface;
use League\OAuth2\Server\ResponseTypes\ResponseTypeInterface;
use Psr\Http\Message\ServerRequestInterface;

class ConfirmationCodeGrant extends AbstractGrant
{
    public function __construct(
        RefreshTokenRepositoryInterface $refreshTokenRepository,
        protected VerifyConfirmationCodeAction $verifyConfirmationCodeAction
    ) {
        $this->setRefreshTokenRepository($refreshTokenRepository);

        $this->refreshTokenTTL = new \DateInterval('P1M');
    }

    public function getIdentifier(): string
    {
        return GrantTypeEnum::CONFIRMATION_CODE_BY_PHONE->value;
    }

    public function respondToAccessTokenRequest(ServerRequestInterface $request, ResponseTypeInterface $responseType, DateInterval $accessTokenTTL): ResponseTypeInterface
    {
        $client = $this->validateClient($request);
        $scopes = $this->validateScopes($this->getRequestParameter('scope', $request));
        $user = $this->validateUser($request->getParsedBody());

        $scopes = $this->scopeRepository->finalizeScopes($scopes, $this->getIdentifier(), $client, $user->getAuthIdentifier());

        $accessToken = $this->issueAccessToken($accessTokenTTL, $client, $user->getAuthIdentifier(), $scopes);
        $refreshToken = $this->issueRefreshToken($accessToken);

        $responseType->setAccessToken($accessToken);
        $responseType->setRefreshToken($refreshToken);

        return $responseType;
    }

    protected function validateUser(array $fields): User
    {
        $confirmationCode = ConfirmationCode::findByPhone($fields['phone']);
        if ($confirmationCode === null) {
            throw AuthUserException::codeError();
        }

        $this->verifyConfirmationCodeAction->execute($confirmationCode, $fields['code']);

        $confirmationCode->delete();

        if (is_null($confirmationCode->user_id)) {
            throw AuthUserException::notFound();
        }

        /** @var ?User $user */
        $user = User::query()->find($confirmationCode->user_id);

        if (is_null($user)) {
            throw AuthUserException::notFound();
        }

        if (!$user->active) {
            throw AuthUserException::blocked();
        }

        return $user;
    }
}
