<?php

namespace App\Http\ApiV1\Modules\Users\Controllers;

use App\Domain\Users\Actions\Users\CreateUserAction;
use App\Domain\Users\Actions\Users\DeleteUserAction;
use App\Domain\Users\Actions\Users\PasswordResetFirstStepUserAction;
use App\Domain\Users\Actions\Users\PasswordResetSecondStepUserAction;
use App\Domain\Users\Actions\Users\PatchUserAction;
use App\Domain\Users\Actions\Users\RefreshPasswordTokenAction;
use App\Domain\Users\Actions\Users\RegisterUserAction;
use App\Exceptions\AuthUserException;
use App\Http\ApiV1\Modules\Users\Queries\UsersQuery;
use App\Http\ApiV1\Modules\Users\Requests\CreateUserRequest;
use App\Http\ApiV1\Modules\Users\Requests\PasswordResetRequest;
use App\Http\ApiV1\Modules\Users\Requests\PasswordResetSecondRequest;
use App\Http\ApiV1\Modules\Users\Requests\PatchUserRequest;
use App\Http\ApiV1\Modules\Users\Requests\RegisterRequest;
use App\Http\ApiV1\Modules\Users\Resources\UsersResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Ensi\CustomersClient\ApiException;
use Illuminate\Contracts\Support\Responsable;
use Illuminate\Http\Request;

class UsersController
{
    public function create(CreateUserRequest $request, CreateUserAction $action): Responsable
    {
        return new UsersResource($action->execute($request->convertToObject()));
    }

    public function patch(int $id, PatchUserRequest $request, PatchUserAction $action): Responsable
    {
        return new UsersResource($action->execute($id, $request->validated()));
    }

    public function delete(int $id, DeleteUserAction $action): Responsable
    {
        $action->execute($id);

        return new EmptyResource();
    }

    public function get(int $id, UsersQuery $query): Responsable
    {
        return new UsersResource($query->findOrFail($id));
    }

    public function search(PageBuilderFactory $pageBuilderFactory, UsersQuery $query): Responsable
    {
        return UsersResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    /**
     * @throws ApiException
     * @throws AuthUserException
     */
    public function register(RegisterRequest $request, RegisterUserAction $action): EmptyResource
    {
        $action->execute($request->convertToObject());

        return new EmptyResource();
    }

    public function searchOne(UsersQuery $query): Responsable
    {
        return new UsersResource($query->firstOrFail());
    }

    public function current(Request $request): Responsable
    {
        return $request->user() ? new UsersResource($request->user()) : new EmptyResource();
    }

    public function refreshPasswordToken(int $usedId, RefreshPasswordTokenAction $action): Responsable
    {
        $action->execute($usedId);

        return new EmptyResource();
    }

    public function passwordResetFirstStep(PasswordResetRequest $request, PasswordResetFirstStepUserAction $action): Responsable
    {
        return new UsersResource($action->execute($request->validated()));
    }

    public function passwordResetSecondStep(PasswordResetSecondRequest $request, PasswordResetSecondStepUserAction $action): Responsable
    {
        return new UsersResource($action->execute($request->validated()));
    }
}
