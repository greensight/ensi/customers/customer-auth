<?php

use App\Domain\Kafka\Actions\Listen\UserPhoneChangeListenAction;
use Ensi\LaravelInitialEventPropagation\RdKafkaConsumerMiddleware;
use Ensi\LaravelMetrics\Kafka\KafkaMetricsMiddleware;

return [
    'global_middleware' => [RdKafkaConsumerMiddleware::class, KafkaMetricsMiddleware::class],
    'stop_signals' => [SIGTERM, SIGINT],

    'processors' => [
        [
            'topic' => 'changes-phone',
            'consumer' => 'default',
            'type' => 'action',
            'class' => UserPhoneChangeListenAction::class,
            'queue' => false,
            'consume_timeout' => 5000,
        ],
    ],

    'consumer_options' => [
       /** options for consumer with name `default` */
       'default' => [
          /*
          | Optional, defaults to 20000.
          | Kafka consume timeout in milliseconds.
          */
         'consume_timeout' => 20000,

          /*
          | Optional, defaults to empty array.
          | Array of middleware.
          */
         'middleware' => [],
       ],
    ],
];
