<?php

namespace App\Domain\Kafka\Actions\Send;

use App\Domain\Kafka\Messages\Send\UserUpdatedEventMessage;
use App\Domain\Users\Models\User;

class SendUserUpdatedEventAction
{
    public function __construct(protected readonly SendKafkaMessageAction $sendAction)
    {
    }

    public function execute(User $user): void
    {
        $event = new UserUpdatedEventMessage($user);
        $this->sendAction->execute($event);
    }
}
