<?php

use App\Domain\Kafka\Actions\Listen\UserPhoneChangeListenAction;
use App\Domain\Kafka\Messages\Listen\Tests\Factories\UserPhoneChangeMessageFactory;
use App\Domain\Users\Models\ConfirmationCode;

use App\Domain\Users\Models\User;

use function Pest\Laravel\assertDatabaseHas;

use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class);
uses()->group('unit');

test("Action UserPhoneChangeListenAction success", function () {
    /** @var IntegrationTestCase $this */

    /** @var User $user */
    $user = User::factory()->create();
    ConfirmationCode::factory()->createQuietly(['user_id' => $user->id]);

    $newPhone = '+78880005500';

    $message = UserPhoneChangeMessageFactory::new()->make([
        'attributes' => [
            'user_id' => $user->id,
            'phone' => $newPhone,
        ],
    ]);

    resolve(UserPhoneChangeListenAction::class)->execute($message);

    assertDatabaseHas((new ConfirmationCode())->getTable(), [
        'user_id' => $user->id,
        'phone' => $newPhone,
    ]);
});
