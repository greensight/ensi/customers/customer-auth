<?php

namespace App\Domain\Users\Actions\Users;

use App\Domain\Users\Models\User;
use Ensi\LaravelPhpRdKafkaProducer\HighLevelProducer;

class RefreshPasswordTokenAction
{
    public function execute(int $userId): void
    {
        /** @var User $user */
        $user = User::query()->findOrFail($userId);
        $user->generatePasswordToken();
        $user->save();

        $message = json_encode([
            'user_id' => $user->id,
            'token' => $user->password_token,
        ]);
        (new HighLevelProducer(config('kafka.topics.generated-password-token')))
            ->sendOne($message);
    }
}
