<?php

namespace App\Http\ApiV1\Modules\Users\Requests;

use App\Domain\Users\Actions\Users\Data\RegisterRequestData;
use App\Http\ApiV1\OpenApiGenerated\Enums\RegisterValidationTypeEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use App\Rules\PhoneRule;
use Illuminate\Validation\Rule;

class RegisterRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'validation_type' => ['required', 'string', Rule::enum(RegisterValidationTypeEnum::class)],
            'login' => ['required', 'email'],
            'password' => ['nullable', 'required_if:validation_type,' . RegisterValidationTypeEnum::PASSWORD->value, 'string'],
            'code' => ['nullable', 'required_if:validation_type,' . RegisterValidationTypeEnum::CODE_BY_PHONE->value, 'string'],
            'first_name' => ['required', 'string'],
            'last_name' => ['nullable', 'string'],
            'phone' => ['required', 'string', new PhoneRule()],
            'birthday' => ['nullable', 'date', 'date_format:Y-m-d', 'before:today'],
        ];
    }

    public function convertToObject(): RegisterRequestData
    {
        return new RegisterRequestData(
            RegisterValidationTypeEnum::from($this->validated('validation_type')),
            $this->validated('login'),
            $this->validated('first_name'),
            $this->validated('phone'),
            $this->validated('password'),
            $this->validated('code'),
            $this->validated('last_name'),
            $this->validated('birthday'),
        );
    }
}
