<?php

namespace App\Domain\Users\Models;

use App\Domain\Users\Models\Tests\Factories\ConfirmationCodeFactory;
use Carbon\CarbonImmutable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * @property int $id
 * @property ?int $user_id
 * @property string $value
 * @property string $phone
 * @property int $attempt
 * @property int $failed_checks_count
 * @property ?CarbonImmutable $end_at
 * @property ?CarbonImmutable $verified_at
 * @property CarbonImmutable $expired_at
 *
 * @property CarbonImmutable $created_at
 * @property CarbonImmutable $updated_at
 */
class ConfirmationCode extends Model
{
    protected $casts = [
        'end_at' => 'datetime',
        'verified_at' => 'datetime',
        'expired_at' => 'datetime',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    public static function makeFromPhone(string $phone): self
    {
        $confirmationCode = new self();

        $confirmationCode->phone = $phone;

        return $confirmationCode;
    }

    public static function findByPhone(string $phone): ?self
    {
        return ConfirmationCode::where('phone', $phone)->first();
    }

    public function updateVerifiedAt(): self
    {
        $this->verified_at = now();

        return $this;
    }

    public function isValid(string $code): bool
    {
        $isValid = $this->value === $code;

        if (!$isValid) {
            $this->updateFailedChecksCount()->save();
        }

        return $isValid;
    }

    private function updateFailedChecksCount(): self
    {
        $this->failed_checks_count++;

        return $this;
    }

    public function generateCode(): self
    {
        $this->value = config('app.confirmation_code.test_code') ?? substr(str_shuffle('0123456789'), 0, 4);
        $this->expired_at = now()->addSeconds(config('app.confirmation_code.expiration_time'));

        $attemptsBeforeBlocked = config('app.confirmation_code.attempts_before_user_is_blocked');
        $this->attempt = $this->attempt >= $attemptsBeforeBlocked ? 1 : $this->attempt + 1;

        $this->end_at = match ($this->attempt) {
            1 => Carbon::now()->addSeconds(config('app.confirmation_code.failed_attempt_1')),
            2 => Carbon::now()->addSeconds(config('app.confirmation_code.failed_attempt_2')),
            $attemptsBeforeBlocked => Carbon::tomorrow(),
            default => $this->end_at,
        };

        return $this;
    }

    public function isBlocked(): bool
    {
        return $this->attempt >= config('app.confirmation_code.attempts_before_user_is_blocked');
    }

    public function isExpired(): bool
    {
        return $this->expired_at->isPast();
    }

    public static function factory(): ConfirmationCodeFactory
    {
        return ConfirmationCodeFactory::new();
    }
}
