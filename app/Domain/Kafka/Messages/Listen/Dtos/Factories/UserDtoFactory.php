<?php

namespace App\Domain\Kafka\Messages\Listen\Dtos\Factories;

use App\Domain\Kafka\Messages\Listen\Dtos\UserDto;
use Ensi\LaravelTestFactories\Factory;

class UserDtoFactory extends Factory
{
    protected function definition(): array
    {
        return [
            'user_id' => $this->faker->modelId(),
            'phone' => $this->faker->numerify('+7##########'),
        ];
    }

    public function make(array $extra = []): UserDto
    {
        return new UserDto($this->makeArray($extra));
    }
}
