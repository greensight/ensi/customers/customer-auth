<?php

namespace App\Domain\Users\Actions\Users;

use App\Domain\Customers\Actions\CreateCustomerAction;
use App\Domain\Users\Actions\ConfirmationCodes\VerifyConfirmationCodeAction;
use App\Domain\Users\Actions\Data\CustomerData;
use App\Domain\Users\Actions\Users\Data\RegisterRequestData;
use App\Domain\Users\Models\ConfirmationCode;
use App\Domain\Users\Models\User;
use App\Exceptions\AuthUserException;
use App\Http\ApiV1\OpenApiGenerated\Enums\RegisterValidationTypeEnum;
use Ensi\CustomersClient\Api\CustomersApi;
use Ensi\CustomersClient\ApiException;
use Ensi\CustomersClient\Dto\SearchCustomersRequest;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class RegisterUserAction
{
    public function __construct(
        protected CreateUserAction $createUserAction,
        protected CreateCustomerAction $createCustomerAction,
        protected RefreshPasswordTokenAction $refreshPasswordTokenAction,
        protected CustomersApi $customersApi,
        protected VerifyConfirmationCodeAction $verifyConfirmationCodeAction
    ) {
    }

    /**
     * @throws ApiException
     * @throws AuthUserException
     */
    public function execute(RegisterRequestData $data): void
    {
        if ($data->validationType == RegisterValidationTypeEnum::CODE_BY_PHONE && !$this->isCodeValid($data->code, $data->phone)) {
            throw AuthUserException::codeError();
        }

        if ($this->isEmailUsed($data->login)) {
            throw AuthUserException::emailIsBusyUser();
        }

        if ($this->isPhoneUsed($data->phone)) {
            throw AuthUserException::phoneIsBusyUser();
        }

        DB::transaction(function () use ($data) {
            $user = $this->createUserAction->execute($data);
            $data->active = $user['active'];

            $customerDto = $this->fillCustomerFields($data, $user->id);

            $this->createCustomerAction->execute($customerDto->toArray());
        });
    }

    /**
     * @throws ApiException
     */
    protected function isPhoneUsed(string $phone): array
    {
        $request = new SearchCustomersRequest(['filter' => ['phone' => $phone]]);

        return $this->customersApi->searchCustomers($request)->getData();
    }

    /**
     * @throws ApiException
     */
    protected function isEmailUsed(string $email): bool
    {
        $userExists = User::query()->where('login', $email)->exists();
        if ($userExists) {
            return true;
        }

        $request = new SearchCustomersRequest(['filter' => ['email' => $email]]);
        $customers = $this->customersApi->searchCustomers($request)->getData();
        if (!empty($customers)) {
            return true;
        }

        return false;
    }

    protected function fillCustomerFields(RegisterRequestData $data, int $userId): CustomerData
    {
        $customerDto = new CustomerData();
        $customerDto->user_id = $userId;
        $customerDto->email = $data->login;
        $customerDto->first_name = $data->firstName;
        $customerDto->last_name = $data->lastName;
        $customerDto->birthday = Carbon::make($data->birthday);
        $customerDto->phone = $data->phone;
        $customerDto->active = $data->active;
        $customerDto->create_by_admin = false;

        return $customerDto;
    }

    private function isCodeValid(?string $code, string $phone): bool
    {
        $confirmationCode = ConfirmationCode::findByPhone($phone);
        if ($confirmationCode === null) {
            return false;
        }

        $this->verifyConfirmationCodeAction->execute($confirmationCode, $code);

        $confirmationCode->delete();

        return true;
    }
}
