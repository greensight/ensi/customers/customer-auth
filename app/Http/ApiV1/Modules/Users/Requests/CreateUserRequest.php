<?php

namespace App\Http\ApiV1\Modules\Users\Requests;

use App\Domain\Users\Actions\Users\Data\CreateUserData;
use App\Domain\Users\Models\User;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class CreateUserRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
         'login' => ['required', Rule::unique(User::class)],
         'active' => ['boolean'],
         'password' => ['required'],
      ];
    }

    public function convertToObject(): CreateUserData
    {
        return new CreateUserData(
            $this->validated('login'),
            $this->validated('password'),
            $this->validated('active'),
        );
    }
}
