<?php

namespace App\Domain\Users\Actions\Users;

use App\Domain\Users\Models\User;

class PatchUserAction
{
    public function execute(int $id, array $fields): User
    {
        /** @var User $user */
        $user = User::query()->findOrFail($id);
        $user->update($fields);

        return $user;
    }
}
