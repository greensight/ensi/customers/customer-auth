<?php

namespace App\Http\ApiV1\Modules\Users\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use App\Rules\PhoneRule;

class PasswordResetRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'phone' => ['nullable', new PhoneRule()],
            'email' => ['nullable'],
        ];
    }
}
