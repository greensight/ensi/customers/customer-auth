<?php

namespace App\Domain\Common\Actions;

use App\Domain\Users\Models\ConfirmationCode;
use App\Domain\Users\Models\User;
use Illuminate\Support\Facades\DB;

class DeleteCustomerPersonalDataAction
{
    public function execute(int $userId): void
    {
        DB::transaction(function () use ($userId) {
            $this->deleteUserData($userId);
            $this->deleteConfirmationCode($userId);
        });
    }

    protected function deleteUserData(int $userId): void
    {
        /** @var User $user */
        $user = User::query()->findOrFail($userId);

        $user->login = null;
        $user->password = null;
        $user->active = false;

        $user->save();
    }

    protected function deleteConfirmationCode(int $userId): void
    {
        $confirmationCodes = ConfirmationCode::query()->where('user_id', $userId)->get();
        $confirmationCodes->each(fn (ConfirmationCode $confirmationCode) => $confirmationCode->delete());
    }
}
