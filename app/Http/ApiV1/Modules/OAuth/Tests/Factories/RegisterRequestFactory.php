<?php

namespace App\Http\ApiV1\Modules\OAuth\Tests\Factories;

use App\Http\ApiV1\OpenApiGenerated\Enums\RegisterValidationTypeEnum;
use Ensi\LaravelTestFactories\BaseApiFactory;

class RegisterRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        $validationType = $this->faker->randomElement(RegisterValidationTypeEnum::cases());

        return [
            'validation_type' => $validationType,
            'login' => $this->faker->email(),
            'password' => $validationType == RegisterValidationTypeEnum::PASSWORD ? $this->faker->word() : null,
            'code' => $validationType == RegisterValidationTypeEnum::CODE_BY_PHONE ? $this->faker->numerify('####') : null,
            'first_name' => $this->faker->name(),
            'last_name' => $this->faker->lastName(),
            'phone' => $this->faker->numerify('+7##########'),
            'birthday' => $this->faker->date(),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }

    public function withCode(string $code, string $phone): self
    {
        return $this->state([
            'validation_type' => RegisterValidationTypeEnum::CODE_BY_PHONE,
            'code' => $code,
            'password' => null,
            'phone' => $phone,
        ]);
    }

    public function withPassword(): self
    {
        return $this->state([
            'validation_type' => RegisterValidationTypeEnum::PASSWORD,
            'code' => null,
            'password' => $this->faker->word(),
        ]);
    }
}
