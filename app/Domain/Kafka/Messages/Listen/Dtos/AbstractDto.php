<?php

namespace App\Domain\Kafka\Messages\Listen\Dtos;

use Illuminate\Database\Eloquent\Concerns\HasAttributes;
use Illuminate\Support\Fluent;

abstract class AbstractDto extends Fluent
{
    use HasAttributes;

    public function __construct($attributes = [])
    {
        parent::__construct($attributes);

        $this->casts = [
            'created_at' => 'datetime',
            'updated_at' => 'datetime',
        ];
    }
}
