<?php

namespace App\Http\ApiV1\Modules\Users\Queries;

use App\Domain\Users\Models\User;
use Ensi\QueryBuilderHelpers\Filters\StringFilter;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class UsersQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(User::query());

        $this->allowedSorts(['id', 'created_at', 'updated_at']);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            ...StringFilter::make('login')->exact()->contain(),
            AllowedFilter::exact('active'),
            AllowedFilter::exact('password_token'),
        ]);

        $this->defaultSort('id');
    }
}
