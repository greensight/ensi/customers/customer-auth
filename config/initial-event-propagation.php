<?php

return [
    'app_code' => 'customers--customer-auth',
    'set_initial_event_http_middleware' => [
        'default_user_type' => '',
        'preserve_existing_event' => true,
        'app_code_header' => '',
        'corelation_id_header' => '',
        'timestamp_header' => '',
    ],
];
