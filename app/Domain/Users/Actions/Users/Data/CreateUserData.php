<?php

namespace App\Domain\Users\Actions\Users\Data;

class CreateUserData
{
    public function __construct(
        public string $login,
        public string $password,
        public bool $active = true
    ) {
    }

    public function toArray(): array
    {
        return [
            'login' => $this->login,
            'password' => $this->password,
            'active' => $this->active,
        ];
    }
}
