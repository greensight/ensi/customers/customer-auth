<?php

namespace App\Exceptions;

use Exception;
use Throwable;

class AuthUserException extends Exception
{
    protected $code = 400;
    protected ?string $textCode;

    public function __construct(string $message = "", string $textCode = null, int $code = 0, ?Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);

        $this->textCode = $textCode;
    }

    public function getTextCode(): ?string
    {
        return $this->textCode;
    }

    public static function notFound(): static
    {
        return new static('Пользователь не найден', 'NOT_FOUND');
    }

    public static function blocked(): static
    {
        return new static('Пользователь заблокирован', 'BLOCKED');
    }

    public static function codeError(): static
    {
        return new static('Неверный код подтверждения', 'CODE_ERROR');
    }

    public static function codeExpired(): static
    {
        return new static('Истек срок действия кода', 'CODE_EXPIRED');
    }

    public static function emailIsBusyUser(): static
    {
        return new static('Пользователь с таким email уже существует', 'EMAIL_IS_BUSY_USER');
    }

    public static function phoneIsBusyUser(): static
    {
        return new static('Пользователь с таким телефоном уже существует', 'PHONE_IS_BUSY_USER');
    }
}
