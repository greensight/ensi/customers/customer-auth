<?php

namespace App\Console\Commands;

use App\Domain\Users\Actions\Users\DeactivateConfirmationCodeAction;
use Illuminate\Console\Command;

class DeactivateOldConfirmationCodesCommand extends Command
{
    protected $signature = 'passwords:deactivate-confirmation-codes';
    protected $description = 'Деактивация кодов подтверждения, срок жизни которых истек';

    public function handle(DeactivateConfirmationCodeAction $action): void
    {
        $action->execute();
    }
}
