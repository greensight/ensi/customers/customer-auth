<?php

namespace App\Domain\Users\Actions\ConfirmationCodes;

use App\Domain\Users\Models\ConfirmationCode;
use App\Exceptions\AuthUserException;

class VerifyConfirmationCodeAction
{
    public function execute(ConfirmationCode $confirmationCode, string $code): void
    {
        $confirmationCode->updateVerifiedAt()->save();

        if ($confirmationCode->isExpired()) {
            throw AuthUserException::codeExpired();
        }

        if (!$confirmationCode->isValid($code)) {
            throw AuthUserException::codeError();
        }
    }
}
